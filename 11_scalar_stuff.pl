#!/usr/bin/perl
#11_scalar_stuff.pl

#String Scalars, Scalar Operations, and Multiline Strings

$var = "This is string scalar!";
$quote = 'I m inside single quote - $var';
$double = "This is inside single quote - $var";

$escape = "This example of escape -\tHello, World!";

print "var = $var\n";
print "quote = $quote\n";
print "double = $double\n";
print "escape = $escape\n";
print "\n";

#
#First does normal print
#Second does literal print (includes var)
#Third interpolates var
#Fourth has t insert a tab



$str = "hello" . "world";	# concatenates strings
$num = 5 + 10;				# adds two numbers
$mul = 4*5;					# multiples two names
$mix = $str . $num; 		# concatenates string and number

print "str = $str\n";
print "num = $num\n";
print "mix = $mix\n";
print "\n";

# First pops out helloworld
# Second pops out num = 15
# Third pops out helloworld15

$string = 'This is
a multiline
string';

print "$string\n";

print <<EOF;
This is
a multiline 
string
EOF

#shows as typed, both of them


