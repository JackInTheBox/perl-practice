#!/usr/bin/perl


$a = 10;


#Double Quotes, works normal interpretation
print "Value of a = $a\n";

#Single quotes, prints literal
print 'Value of a = $a\n';