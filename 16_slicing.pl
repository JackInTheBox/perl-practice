#!/usr/bin/perl

#16_slicing.pl


#Slicing arrays
#	This selects more than one item from an array to produce another array

@days = qw/Mon Tue Wed Thu Fri Sat Sun/;
@weekdays = @days[0,5,6];
print "@weekdays\n";
print "\n";

#Slicing can use any valid indices

@weekdays2 = @days[3..5];
print "@weekdays2\n";
