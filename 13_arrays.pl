#!/usr/bin/perl
#13_arrays.pl

@ages = (25,26,27);
@names = ("Jackson", "Kumar", "Macleish");

print "\$ages[0] = $ages[0]\n";
print "\$ages[1] = $ages[1]\n";
print "\$ages[2] = $ages[2]\n";
print "\n";

print "\$names[0] = $names[0]\n";
print "\$names[1] = $names[1]\n";
print "\$names[2] = $names[2]\n";
print "\n";

#Access array elements

@days = qw/Mon Tue Wed Thu Fri Sat Sun/;

print "$days[0]\n";		#Mon
print "$days[1]\n";		#Tue
print "$days[2]\n";		#Wed
print "$days[6]\n";		#Sun
print "$days[-1]\n";	#Sun
print "$days[-7]\n";	#Mon


