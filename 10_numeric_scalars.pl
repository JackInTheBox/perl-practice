#!/usr/bin/perl

#numeric scalars

$integer = 200;
$negative = -300;
$floating = 200.190;
$big_float = -1.2E-23;

# Octal 377 = 255 decimal
$octal = 0377;

# Hexadecimal FF = 255 decimal
$hexa = 0xff;

print "integer = $integer\n";
print "negative = $negative\n";
print "floating = $floating\n";
print "big_float = $big_float\n";
print "octal = $octal\n";
print "hexa = $hexa\n";

= Results

Large float shows it as -1.2e-23
Float shows the value as 200.19 (cuts off last trailing 0)
Octal and Hexa show 255 (decimal values)

=cut