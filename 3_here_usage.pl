#!/usr/bin/perl

$a = 10;

$var = << "EOF";
This is the syntax for here document and it will continue
until it encounters a EOF in the first line. 
This case of double quot so the variable value will be interpolated.

For example, value of a = $a
EOF

print "$var\n";

$var = << 'EOF';
This si the case of single quote.
Thus the value is not interpolated.

For example, value of a = $a
EOF
print "$var\n"