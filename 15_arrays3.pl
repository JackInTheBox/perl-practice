#!/usr/bin/perl

#15_arrays3.pl

#Create a simple array
@coins = ("Quarter", "Dime", "Penny");
print "1. \@coins = @coins\n";

#add one element at the end of the array
push(@coins, "Nickel");
print "Add to end\n";
print "2. \@coins = @coins\n";

#add one element at the beginning of the array
unshift(@coins, "Half Dollar");
print "Add to beginning\n";
print "3. \@coins = @coins\n";

#Remove one element from the last of the array
pop(@coins);
print "Remove last one\n";
print "4. \@coins = @coins\n";

#remove one element from the beginning of the array
shift(@coins);
print "Remove first one\n";
print "5. \@coins = @coins\n";



