#!/usr/bin/perl
#12_scalar_stuff_2.pl

# More Scalar Stuff

# V-Strings
#Forms V-based literals

$smile = v9786;						# Smile Emoji
$foo = v102.111.111;				# Spells Foo
$martin = v77.97.114.116.105.110;	# Spells Martin

print "smile = $smile\n";
print "foo = $foo\n";
print "martin = $martin\n";
print "\n";

#
# Special Literals
#

print "File name ". __FILE__ . "\n";
print "Line Number ". __LINE__ . "\n";
print "Package " . __PACKAGE__ . "\n";

#they cannot be interpolated
print "__FILE__ __LINE__ __PACKAGE__\n";
