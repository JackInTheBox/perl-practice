#!/usr/bin/perl

#14_arrays2.pl

@var_10 = (1..10);
@var_20 = (10..20);
@var_abc = (a..z);

print "@var_10\n";
print "@var_20\n";
print "@var_abc\n";
print "\n";

@array = (1,2,3);
@array[50] = 4;

$size = @array;
$max_index = $#array;

print "Size: $size\n";
print "Max Index: $max_index\n";
