#!/usr/bin/perl

#Perl treads variables differently based on context
#Here (1) is an array, and (2) is a scalar

@names = ('Danielle', 'Bryan', 'Justin');

@copy = @names;
$size = @names;

print "The listed names are : @copy\n";
print "Number of names : $size\n";

=Produces:
Given names are : Danielle Bryan Justin
Number of names are : 3
=cut
