#!/usr/bin/perl

# define Strings
$var_string = "Rain-Drops-On-Roses-And-Whiskers-On-Kittens";
$var_names = "Larry,David,Roger,Ken,Michael,Tom";

# transform above strings into arrays.
@string = split('-', $var_string);
@names  = split(',', $var_names);

print "$string[3]\n";  # This will print Roses
print "$names[4]\n";   # This will print Michael

print "\n";
# Now we'll use the join function

$string1 = join( '-', @string);
$string2 = join( ',', @names); 	#Makes a string of @name items seperated by commas

print "$string1\n";
print "$string2\n";